typedef struct FUNCIONARIO{
    int  codigo;
    char nome[50];
    int idade;
    char empresa [20];
    char departamento[40];
    float salario;
} FUNCIONARIO;

int menuPrincipal();

/** Arvore Rubro negra**/

typedef struct NO *arvoreLLRB;
arvoreLLRB *cria_arvoreLLRB();
int vazia_arvoreLLRB(arvoreLLRB *raiz);
int cor(struct NO *H);
void trocaCor(struct NO *H);
struct NO *rotacionaEsquerda(struct NO *A);
struct NO *rotacionaDireita(struct NO *A);
struct NO *move2EsqRED(struct NO *H);
struct NO *move2DirRED(struct NO *H);
struct NO *balancear(struct NO *H);
int altura_arvoreLLRB(arvoreLLRB *raiz);
int totalNO_arvoreLLRB(arvoreLLRB *raiz);
int insere_arvoreLLRB(arvoreLLRB *raiz, struct FUNCIONARIO valor);
int consuta_arvoreLLRB(arvoreLLRB *raiz,int valor);
void preOrdem_arvoreLLRB(arvoreLLRB *raiz);
void emOrdem_arvoreLLRB(arvoreLLRB *raiz);
void posOrdem_arvoreLLRB(arvoreLLRB *raiz);
int remove_arvoreLLRB(arvoreLLRB *raiz, int valor);
void liberar_arvoreLLRB(arvoreLLRB *raiz);



/** Arvore AVL **/
typedef struct NO *arvAVL;
arvAVL *cria_arvAVL();
int vazia_arvAVL(arvAVL *raiz);
int altura_arvAVL(arvAVL *raiz);
int totalNO_arvAVL(arvAVL *raiz);
int insere_arvAVL(arvAVL *raiz,struct FUNCIONARIO valor);
int consuta_arvAVL(arvAVL *raiz,int valor);
int remove_arvAVL(arvAVL *raiz,int valor);
void preOrdem_arvAVL(arvAVL *raiz);
void emOrdem_arvAVL(arvAVL *raiz);
void posOrdem_arvAVL(arvAVL *raiz);
void liberar_arvAVL(arvAVL *raiz);
