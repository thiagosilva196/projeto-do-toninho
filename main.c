#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arvore.h"
#include <locale.h>
#include<windows.h>


main()
{
    setlocale(LC_ALL,"Portuguese");
    int x, AVL, RB , opc , i = 0;
    /**Desorndenado**/
    LARGE_INTEGER tempoInicial, tempoFinal, freq, tempoInicial2, tempoFinal2, freq2,tempoInicial3, tempoFinal3, freq3,tempoInicial4, tempoFinal4, freq4;
    /**Ordenado**/
    LARGE_INTEGER tempoInicial5, tempoFinal5, freq5, tempoInicial6, tempoFinal6, freq6,tempoInicial7, tempoFinal7, freq7,tempoInicial8, tempoFinal8, freq8;
    float tempoTotal, tempoTotal2,tempoTotal3,tempoTotal4,tempoTotal5,tempoTotal6,tempoTotal7,tempoTotal8;
    opc = menuPrincipal();
    arvAVL *raiz;
    arvoreLLRB *raizRB;
    raiz = cria_arvAVL();
    raizRB = cria_arvoreLLRB();

   const char s[2] = ";";
   char *token;
   char linha[90];
   char *result;
   FUNCIONARIO *funcionario;
   funcionario = (FUNCIONARIO*) malloc(15000 * sizeof(FUNCIONARIO));

   FILE *arq;
   QueryPerformanceCounter(&tempoInicial);
   QueryPerformanceCounter(&tempoInicial3);
   if((arq = fopen("massaDados.csv", "r")) == NULL)
   {
       printf("Erro ao abrir o arquivo.\n");
   }
   token = strtok(arq, s);

   while (!feof(arq))
   {
      result = fgets(linha, 90, arq);


      if (result)
          token = strtok(result, ";");

      int j = 0;
      while( token != NULL )
      {

        switch(j)
        {
            case 0:
                funcionario[i].codigo = atoi(token);
                //printf("%d\n", funcionario[i].codigo);
                break;
            case 1:
                if(NULL != token){
                    strcpy(funcionario[i].nome, token);
                    //printf("%s\n", funcionario[i].nome);

                }
                break;
            case 2:
                funcionario[i].idade = atoi(token);
                //printf("%d\n", funcionario[i].idade);
                break;
            case 3:
                 if(NULL != token){
                    strcpy(funcionario[i].empresa, token);
                    //printf("%s\n", funcionario[i].empresa);
                }
                break;
            case 4:
                 if(NULL != token){
                    strcpy(funcionario[i].departamento, token);
                    //printf("%s\n", funcionario[i].departamento);

                }
                break;
            case 5:
                funcionario[i].salario = atof(token);
                //printf("%.2f\n\n", funcionario[i].salario);
                break;
        }


        token = strtok(NULL, s);
        j++;
      }
      i++;
  }


   switch(opc){
   case 1:
      i = 0;

      while(i<15000){
        /**Tempo Apenas da inser��o dos dados AVL**/
        QueryPerformanceCounter(&tempoInicial2);
        x = insere_arvAVL(raiz,funcionario[i]);
        i++;
        QueryPerformanceCounter(&tempoFinal);
        QueryPerformanceCounter(&tempoFinal2);
      }
        /** Desde a abertura do arquivo AVL **/
        QueryPerformanceFrequency(&freq);
        tempoTotal = (float)(tempoFinal.QuadPart - tempoInicial.QuadPart)/freq.QuadPart;


        /**Tempo Apenas da inser��o dos dados AVL**/
        QueryPerformanceFrequency(&freq2);
        tempoTotal2 = (float)(tempoFinal2.QuadPart - tempoInicial2.QuadPart)/freq2.QuadPart;

        i = 0;
      while(i<15000){
        /**Tempo Apenas da inser��o dos dados AVL**/
          QueryPerformanceCounter(&tempoInicial4);
          x = insere_arvoreLLRB(raizRB,funcionario[i]);
          i++;
           QueryPerformanceCounter(&tempoFinal3);
           QueryPerformanceCounter(&tempoFinal4);

      }
      /** Desde a abertura do arquivo RUBRO NEGRA **/
      QueryPerformanceFrequency(&freq3);
     tempoTotal3 = (float)(tempoFinal3.QuadPart - tempoInicial3.QuadPart)/freq3.QuadPart;

      /**Tempo Apenas da inser��o dos dados AVL**/
        QueryPerformanceFrequency(&freq4);
        tempoTotal4 = (float)(tempoFinal4.QuadPart - tempoInicial4.QuadPart)/freq4.QuadPart;

      printf("\n");
      AVL = altura_arvAVL(raiz);
      printf("\nAltura da arvore AVL: %d\n",AVL);
      AVL = totalNO_arvAVL(raiz);
      printf("\nTotal de n�s na arvore AVL: %d\n",AVL);
      RB = altura_arvoreLLRB(raizRB);
      printf("\nAltura da arvore Rubro Negra: %d\n ",RB);
      RB = totalNO_arvoreLLRB(raizRB);
      printf("\nTotal de n�s na arvore Rubro Negra: %d\n",RB);
      printf("\n Tempo desde a abertura do arquivo e inser��o desordenado AVL %f\n",tempoTotal);
      printf("\n Tempo apenas da inser��o desordenado AVL %f\n",tempoTotal2);
      printf("\n Tempo desde a abertura e da inser��o desordenado RUBRO NEGRA %f\n",tempoTotal3);
      printf("\n Tempo apenas da inser��o desordenado RUBRO NEGRA %f\n",tempoTotal4);



      printf("\n");
      fclose(arq);

    break;

   case 2:
    QueryPerformanceCounter(&tempoInicial5);
    QueryPerformanceCounter(&tempoInicial7);
    radixsort(funcionario, 15000);
    fclose(arq);
    if((arq = fopen("massaDadosOrdenados.csv", "w")) == NULL)
   {
       printf("Erro ao abrir o arquivo.\n");
   }
   for(i = 0; i < 15000; i++){
        fprintf(arq, "%d; %s; %d; %s; %s; %.2f\n", funcionario[i].codigo, funcionario[i].nome, funcionario[i].idade,
               funcionario[i].empresa, funcionario[i].departamento, funcionario[i].salario);
    }
    i = 0;
      while(i<15000){
            /**Tempo apenas da inser��o AVL**/
        QueryPerformanceCounter(&tempoInicial6);
        x = insere_arvAVL(raiz,funcionario[i]);
        i++;
         QueryPerformanceCounter(&tempoFinal5);
         QueryPerformanceCounter(&tempoFinal6);

      }
      /**Desde a abertura do arquivo AVL**/
      QueryPerformanceFrequency(&freq5);
      tempoTotal5 = (float)(tempoFinal5.QuadPart - tempoInicial5.QuadPart)/freq5.QuadPart;

      /**Tempo apenas da inser��o AVL**/
      QueryPerformanceFrequency(&freq6);
      tempoTotal6= (float)(tempoFinal6.QuadPart - tempoInicial6.QuadPart)/freq6.QuadPart;

        i = 0;
      while(i<15000){
            /**Tempo apenas da inser��o RUBRO NEGRA**/
          QueryPerformanceCounter(&tempoInicial8);
          x = insere_arvoreLLRB(raizRB,funcionario[i]);
          i++;
          QueryPerformanceCounter(&tempoFinal7);
          QueryPerformanceCounter(&tempoFinal8);
      }
      /**Desde a abertura do arquivo RUBRO NEGRA**/
       QueryPerformanceFrequency(&freq7);
      tempoTotal7 = (float)(tempoFinal7.QuadPart - tempoInicial7.QuadPart)/freq7.QuadPart;

        /**Tempo apenas da inser��o RUBRO NEGRA**/
      QueryPerformanceFrequency(&freq8);
      tempoTotal8 = (float)(tempoFinal8.QuadPart - tempoInicial8.QuadPart)/freq8.QuadPart;


      printf("\n");
      AVL = altura_arvAVL(raiz);
      printf("\nAltura da arvore AVL:%d\n ",AVL);
      AVL = totalNO_arvAVL(raiz);
      printf("\nTotal de n�s na arvore AVL: %d\n",AVL);
      RB = altura_arvoreLLRB(raizRB);
      printf("\nAltura da arvore Rubro Negra: %d\n ",RB);
      RB = totalNO_arvoreLLRB(raizRB);
      printf("\nTotal de n�s na arvore Rubro Negra: %d\n",RB);
      printf("\n Tempo desde a abertura, ordena��o e da inser��o ordenado AVL %f\n",tempoTotal5);
       printf("\n Tempo desde a inser��o ordenado AVL %f\n",tempoTotal6);
       printf("\n Tempo desde a abertura, ordena��o e da inser��o ordenado RUBRO NEGRA %f\n",tempoTotal7);
       printf("\n Tempo desde a inser��o ordenado RUBRO NEGRA %f\n",tempoTotal8);
      printf("\n");



      fclose(arq);

    break;


    default:
     printf("\nOp��o invalida!\n\n");

   }



  return(0);

}

void radixsort(FUNCIONARIO funcionario[], int tamanho) {

    int i;
    FUNCIONARIO *b;
    int maior = funcionario[0].codigo;
    int exp = 1;

    b = (FUNCIONARIO*)calloc(tamanho, sizeof(FUNCIONARIO));

    for (i = 0; i < tamanho; i++) {     // Atribui o maior valor do vetor para a variavel 'maior'
        if (funcionario[i].codigo > maior)
    	    maior = funcionario[i].codigo;
    }

    while (maior/exp > 0) {

        int bucket[10] = { 0 };

        for (i = 0; i < tamanho; i++)
    	    bucket[(funcionario[i].codigo / exp) % 10]++;                // Efetua o incremento no vetor bucket

        for (i = 1; i < 10; i++)
    	    bucket[i] += bucket[i - 1];                     // Efetua a contagem do posicionamento, somando com o anterior

        for (i = tamanho - 1; i >= 0; i--)
    	    b[--bucket[(funcionario[i].codigo / exp) % 10]] = funcionario[i];  //  Atribui no vetor auxiliar a partir da posi��o armazenada no bucket

        for (i = 0; i < tamanho; i++)
            funcionario[i] = b[i];                                //  Atribui o valor ao vetor original
    	exp *= 10;                                          //  Aumenta a base a ser comparada
    }

    //printf("VetorOrd\t=\t{");


   // printf("%d}\n\n", funcionario[tamanho - 1]);

    free(b);
}









